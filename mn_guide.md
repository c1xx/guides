# ARGO masternode guide (step by step)

## Before we start, 3 important things that every user should have/do

### 1. Wallet password
Have an encrypted wallet is not just recommended, but should be done by every wallet user. If anyone gets your wallet.dat and is not encrypted, then he will be able to move your coins from your wallet away.
This is not a 100% protection, but an encrypted wallet with a good / strong password is more difficult to get hacked.
To set a wallet password, follow this steps:

1. Open your wallet, goto **Settings** -> **Encrypt Wallet**

    <img src="https://node-support.network/coins/argo/mn-guide/3.png">
    
2. Set your password (do me a favor and **use strong password!**) and confirm with **OK button**

    <img src="https://node-support.network/coins/argo/mn-guide/4.png">
    
    confirm next window with **YES button**
    
    <img src="https://node-support.network/coins/argo/mn-guide/5.png">
    
    and last window with **OK button**
    
    <img src="https://node-support.network/coins/argo/mn-guide/6.png">

### 2. Wallet private key
Owning your wallet private key is very important to be able to restore your coins without backup. To get your wallet private key, follow this steps:
1. Open your wallet, goto **File** -> **Receiving addresses**

    <img src="https://node-support.network/coins/argo/mn-guide/1.png">

    and copy your **main wallet** address.
    
    <img src="https://node-support.network/coins/argo/mn-guide/2.png">

    If you started your ARGO wallet 1st time, your main wallet label should be **(no label)**, you can everytime rename it --> Just double-click on label and set your label.

2. Open Debug console (goto **Tools** -> **Debug console**)

    <img src="https://node-support.network/coins/argo/mn-guide/7.png">
    
    and type
    
    `dumprivkey <yourwalletaddress>`
    
### 3. Backup of wallet
